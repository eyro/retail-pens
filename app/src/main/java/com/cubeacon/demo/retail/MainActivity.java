package com.cubeacon.demo.retail;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.eyro.cubeacon.CBActivity;
import com.eyro.cubeacon.CBBeacon;

public class MainActivity extends CBActivity {

    @Override
    protected void onEnteredBeacon(CBBeacon beacon) {
        super.onEnteredBeacon(beacon);
    }

    @Override
    protected void onExitedBeacon(CBBeacon beacon, long timeInterval) {
        super.onExitedBeacon(beacon, timeInterval);
    }

    @Override
    protected void onNearBeacon(CBBeacon beacon) {
        super.onNearBeacon(beacon);
    }

    @Override
    protected void onFarBeacon(CBBeacon beacon) {
        super.onFarBeacon(beacon);
    }

    @Override
    protected void onImmediateBeacon(CBBeacon beacon) {
        super.onImmediateBeacon(beacon);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

}

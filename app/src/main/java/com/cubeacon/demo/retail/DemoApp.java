package com.cubeacon.demo.retail;

import android.app.Application;

import com.eyro.cubeacon.CBApp;

/**
 * Created by mohammad on 11/13/2015.
 */
public class DemoApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        CBApp.setup(this, "cubeacon.properties");
        CBApp.enableDebugLogging(true);
    }
}
